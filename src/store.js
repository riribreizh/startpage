import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
  user: {
    email: 'richard@houbathecat.fr',
    display: 'Richard'
  },
  pages: {
    'home': {
      title: 'Page d\'accueil',
      default: true,
      columns: [
        // each column contains one to many widgets (rows of card)
        {
          id: 1,
          rows: [
            { id: '8rFeiQ' }
          ],
        },
        {
          id: 2,
          rows: [
            { id: 'To62dA' },
            { id: '93FrHE' },
            { id: 'I4e0bg' },
          ],
        },
        {
          id: 3,
          rows: [],
        },
      ],
    },
    '2': {
      title: 'Autre',
      columns: [
        // { id: 'OE94ej', rows: [] },
        // { id: 'z394Sa', rows: [] }
      ]
    }
  },
  widgets: {
    '8rFeiQ': {
      title: 'Courriel',
      type: 'undef-widget',
      definition: {},
    },
    'To62dA': {
      title: 'Météo',
      type: 'undef-widget',
      definition: {},
    },
    '93FrHE': {
      title: 'Cloud',
      type: 'undef-widget',
      definition: {},
    },
    'I4e0bg': {
      title: 'LinuxFR',
      type: 'undef-widget',
      definition: {},
    }
  },
  data: {
    '8rFeiQ': {
    },
    'To62dA': {
    },
    '93FrHE': {
    },
    'I4e0bg': {
    }
  }
}

const getters = {
  rowsOf: (state) => (page, colnum) => {
    return state.pages[page].columns[colnum].rows
  },
}
const mutations = {
  setColumn (state, context) {
    state.pages[context.page].columns[context.colnum].rows = context.data
  }
}

const actions = {}

export default new Vuex.Store({
  // eslint-disable-next-line no-undef
  strict: process.env.NODE_ENV !== 'production',
  state,
  getters,
  mutations,
  actions
})
