// imported and updated to es6 from https://github.com/simplesmiler/vue-clickaway
// the original does not seem maintained anymore and the v-on-clickaway isn't recognized

import Vue from 'vue'

const HANDLER = '_clickaway_handler'
const BINDING = 'clickaway'

function bind (el, binding, vnode) {
  unbind(el)

  const callback = binding.value
  if (typeof callback !== 'function') {
    // eslint-disable-next-line no-undef
    if (process.env.NODE_ENV !== 'production') {
      Vue.util.warn(`v-${BINDING}="${binding.expression}" expects a function, got ${callback}`)
    }
    return
  }

  // @NOTE: Vue binds directives in microtasks, while UI events are dispatched
  //        in macrotasks. This causes the listener to be set up before
  //        the "origin" click event (the event that lead to the binding of
  //        the directive) arrives at the document root. To work around that,
  //        we ignore events until the end of the "initial" macrotask.
  // @REFERENCE: https://jakearchibald.com/2015/tasks-microtasks-queues-and-schedules/
  // @REFERENCE: https://github.com/simplesmiler/vue-clickaway/issues/8
  let initialMacrotaskEnded = false
  setTimeout(() => initialMacrotaskEnded = true, 0)
  

  el[HANDLER] = (ev) => {
    // @NOTE: this test used to be just `el.containts`, but working with path is better,
    //        because it tests whether the element was there at the time of
    //        the click, not whether it is there now, that the event has arrived
    //        to the top.
    // @NOTE: `.path` is non-standard, the standard way is `.composedPath()`
    const path = ev.path || (ev.composedPath ? ev.composedPath() : undefined)
    if (initialMacrotaskEnded && (path ? path.indexOf(el) < 0 : !el.contains(ev.target()))) {
      return callback.call(vnode.context, ev)
    }
  }
  document.documentElement.addEventListener('click', el[HANDLER], false)
}

function unbind (el) {
  document.documentElement.removeEventListener('click', el[HANDLER], false)
  delete el[HANDLER]
}

export default {
  directives: {
    [BINDING]: {
      bind,
      unbind,
    }
  }
}
