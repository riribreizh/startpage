import Vue from 'vue'
//import Multiselect from 'vue-multiselect'
import MdIcon from './MdIcon.vue'
import Drawer from './Drawer.vue'
import Dropdown from './Dropdown.vue'

// dynamic import of all components declared in the following array
[
  MdIcon,
  Drawer,
  Dropdown,
].forEach(item => {
  Vue.component(item.name, item)
})

//Vue.component(Multiselect)
