import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import Page from './app/Page.vue'

Vue.use(Router)

const routes = []
let defaultPage = ''

// build routes from existing pages, determining the default one
Object.keys(store.state.pages).forEach(id => {
  const page = store.state.pages[id]
  if (page.default === true && !defaultPage) {
    defaultPage = id
  }
  routes.push({ path: '/' + id, name: id, component: Page })
})

// if no computed default page, use the first route
if (!defaultPage) {
  defaultPage = routes[0].name
}

// add home route redirected to the computed default page
routes.splice(0, 0, { path: '/', redirect: defaultPage })

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})
export default router

router.beforeEach((to, from, next) => {
  // ensure the requested page exists
  let doNext = undefined
  if (to.name === '') {
    // TODO unknown page, create it ?
    doNext = false
  }
  next(doNext)
})
