import Vue from 'vue'
import './components/global'
import store from './store'
import router from './router'
import App from './app/App.vue'

Vue.config.productionTip = false

new Vue(Object.assign(App, {
  store,
  router
}))
